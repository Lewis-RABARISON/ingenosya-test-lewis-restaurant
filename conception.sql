-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 07 oct. 2021 à 11:23
-- Version du serveur :  10.4.17-MariaDB
-- Version de PHP : 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `conception`
--

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20211006130042', '2021-10-06 15:00:49', 659),
('DoctrineMigrations\\Version20211007014132', '2021-10-07 03:41:43', 11697),
('DoctrineMigrations\\Version20211007065650', '2021-10-07 08:57:10', 922),
('DoctrineMigrations\\Version20211007072052', '2021-10-07 09:21:11', 910);

-- --------------------------------------------------------

--
-- Structure de la table `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL,
  `ingredient_libelle` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ingredient_unite` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ingredient_quantite` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ingredient`
--

INSERT INTO `ingredient` (`id`, `ingredient_libelle`, `ingredient_unite`, `ingredient_quantite`) VALUES
(14, 'Vary', 'g', 7),
(15, 'Sira', 'Kg', 10);

-- --------------------------------------------------------

--
-- Structure de la table `repas`
--

CREATE TABLE `repas` (
  `id` int(11) NOT NULL,
  `repas_prix` int(11) NOT NULL,
  `repas_designation` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `repas`
--

INSERT INTO `repas` (`id`, `repas_prix`, `repas_designation`) VALUES
(1, 2000, 'Hamburger'),
(3, 2000, 'Hamburger 2'),
(6, 2000, 'Hamburger 2');

-- --------------------------------------------------------

--
-- Structure de la table `repas_ingredient`
--

CREATE TABLE `repas_ingredient` (
  `repas_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `repas_ingredient`
--

INSERT INTO `repas_ingredient` (`repas_id`, `ingredient_id`) VALUES
(6, 14),
(6, 15);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_6BAF7870A5E10C9F` (`ingredient_libelle`);

--
-- Index pour la table `repas`
--
ALTER TABLE `repas`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `repas_ingredient`
--
ALTER TABLE `repas_ingredient`
  ADD PRIMARY KEY (`repas_id`,`ingredient_id`),
  ADD KEY `IDX_CC79FC391D236AAA` (`repas_id`),
  ADD KEY `IDX_CC79FC39933FE08C` (`ingredient_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `repas`
--
ALTER TABLE `repas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `repas_ingredient`
--
ALTER TABLE `repas_ingredient`
  ADD CONSTRAINT `FK_CC79FC391D236AAA` FOREIGN KEY (`repas_id`) REFERENCES `repas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_CC79FC39933FE08C` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
