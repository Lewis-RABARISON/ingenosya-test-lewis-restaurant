# Application Web de gestion de restaurant

### Configuration requise et installation
* Exigences
```
- PHP 7.3 ou version supérieure
- Node.Js 14.17 ou version supérieure
```

* Installation
```
- composer install
- npm install
```

### Exécution de l'application

* Pour exécuter l'application côté frontend
```
- Exécutez cette commande : ng serve --open
```

* Pour exécuter l'application côté backend 
```
- Exécutez cette commande : php -S 127.0.0.1:8000 -t public
```

### Fonctionnalités

```
- Gérer les ingredients
- Gérer les repas
- Voir le détail du repas
```
