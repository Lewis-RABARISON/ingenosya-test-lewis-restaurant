import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IngredientService } from 'src/app/service/ingredient.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  ingredientForm = new FormGroup({
    ingredientLibelle: new FormControl(''),
    ingredientUnite: new FormControl(''),
    ingredientQuantite: new FormControl(''),
  });

  constructor(private formBuilder: FormBuilder, private router: Router, 
              private ingredientService: IngredientService) { }

  ngOnInit()
  {
    this.validationIngredientForm();
  }

  /**
   * Retour à la liste des ingredients
   */
  listIngredient()
  {
    this.router.navigate(["/ingredient"]);
  }

  /**
   * Validation de l'ajout d'un ingredient
   */
  validationIngredientForm()
  {
    this.ingredientForm = this.formBuilder.group({
      ingredientLibelle : ['',Validators.required],
      ingredientUnite : [''],
      ingredientQuantite : [''],
    });
  }

  /**
   * Soumission du formulaire
   */
  onSubmitIngredientForm()
  {
    let data = this.ingredientForm.value;
    this.ingredientService.addIngredientService(data)
                          .then((data) => {
                              this.listIngredient();
                          })
                          .catch((error) => {
                            console.log("L'enregistrement a echoué");
                            console.log(error);
                          });                     
  }
}
