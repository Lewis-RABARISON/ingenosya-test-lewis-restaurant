import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IngredientService } from 'src/app/service/ingredient.service';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})


export class IngredientComponent implements OnInit {

  ingredient_columns: string[] = ['Ingredient','Unité','Quantité','Actions']; 
  ingredients_data: any = [];

  constructor(private ingredientService: IngredientService, private router: Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getAllIngredients();
  }

  /**
  * Recupère la liste des ingredients
  */
  getAllIngredients()
  {
    this.ingredientService.getAllIngredientsService()
                          .then((res) => {
                            this.ingredients_data = res;
                          })
                          .catch((error) => {
                            console.log("Echec de la recuperation de la liste");
                            console.log(error);
                          });                       
  }

  /**
   * Retour à la liste des ingredients
   */
   listIngredient()
   {
     this.router.navigate(["/ingredient"]);
   }

  /**
   * Ajouter un ingredient
  */
  addIngredient()
  {
    this.router.navigate(["/ingredient/ajout"]);
  }

  /**
   * Supprimer un ingredien
   * @param id 
   */
  deleteIngredient(id:number)
  {
    this.ingredientService.deleteIngredientService(id)
                          .then((id) => {
                            this.ingredients_data = this.ingredients_data.filter((ingredient:any) => {ingredient.id != id});
                          })
                          .catch((error) => {
                            console.log("La suppression a echoué");
                            console.log(error);
                          });
  }
}
