import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IngredientService } from 'src/app/service/ingredient.service';
import { IngredientModel } from 'src/app/Model/IngredientModel/ingredientModel';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  id: any;
  dataIngredientEdited: any;
  ingredient = new IngredientModel('',0,0,0);

  editIngredientForm = new FormGroup({
    ingredientLibelle: new FormControl(''),
    ingredientUnite: new FormControl(''),
    ingredientQuantite: new FormControl(''),
  });

  constructor(private route: ActivatedRoute,private ingredientService: IngredientService, 
              private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void 
  {
    this.id = this.route.snapshot.params.id;
    this.getIngredient();
  }

  /**
   * Retour à la liste des ingredients
   */
   listIngredient()
   {
     this.router.navigate(["/ingredient"]);
   }

  /**
   * Edite un ingredient
   */
  getIngredient()
  {
    this.ingredientService.getIngredientService(this.id)
                          .then((res) => {
                            this.dataIngredientEdited = res;
                            this.ingredient = this.dataIngredientEdited;
                          })
                          .catch((error) => {
                            console.log("Echec de l'edite de l'ingredient");
                            console.log(error);
                          });
  }

    /**
    * Soumission du formulaire
    */
    onSubmitEditIngredientForm()
    {
      let data = this.editIngredientForm.value;
      this.ingredientService.updateIngredientService(data, this.id)
                          .then((data) => {
                              this.listIngredient();
                          })
                          .catch((error) => {
                            console.log("La modification a echoué");
                            console.log(error);
                          });    
    }
}
