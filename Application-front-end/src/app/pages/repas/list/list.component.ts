import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RepasService } from 'src/app/service/repas.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  repas_columns: string[] = ['Repas','Ingredient','Actions']; 
  repas_data: any = [];
  
  constructor(private repasService: RepasService, private router: Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getAllRepas();
  }

  /**
  * Recupère la liste des repas
  */
  getAllRepas()
  {
    this.repasService.getAllRepasService()
                      .then((res) => {
                               this.repas_data = res;
                             })
                      .catch((error) => {
                              console.log("Echec de la recuperation de la liste");
                              console.log(error);
                            });                       
  }

  /**
   * Ajouter un repas
  */
  addMeal()
  {
    this.router.navigate(["/repas/ajout"]);
  }

  /**
   * Supprimer un repas
   * @param id 
   */
   deleteMeal(id:number)
   {
     this.repasService.deleteRepasService(id)
                           .then((id) => {
                             this.repas_data = this.repas_data.filter((repas:any) => {repas.id != id});
                           })
                           .catch((error) => {
                             console.log("La suppression a echoué");
                             console.log(error);
                           });
   }
}
