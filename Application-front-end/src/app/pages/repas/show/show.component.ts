import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RepasService } from 'src/app/service/repas.service';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {

  id: any;
  dataMealEdited: any;

  constructor(private route: ActivatedRoute, private router: Router, private repasService: RepasService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this. getMeal();
  }

  /**
 * Edite un repas
 */
  getMeal() {
    this.repasService.getRepasService(this.id)
      .then((res) => {
        this.dataMealEdited = res;
      })
      .catch((error) => {
        console.log("Echec de l'edite du repas");
        console.log(error);
      });
  }
}
