import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IngredientComponent } from './pages/ingredient/list/ingredient.component';
import { AddComponent } from './pages/ingredient/add/add.component';
import { EditComponent } from './pages/ingredient/edit/edit.component';
import { ListComponent } from './pages/repas/list/list.component';
import { AddMealComponent } from './pages/repas/add-meal/add-meal.component';
import { EditMealComponent } from './pages/repas/edit-meal/edit-meal.component';
import { ShowComponent } from './pages/repas/show/show.component';


const routes: Routes = [
  {path:"ingredient", component: IngredientComponent},
  {path:"ingredient/ajout", component: AddComponent},
  {path:"ingredient/:id/editer", component: EditComponent},
  {path:"repas", component: ListComponent},
  {path:"repas/ajout", component: AddMealComponent},
  {path:"repas/:id/editer", component: EditMealComponent},
  {path:"repas/:id/voir", component: ShowComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
