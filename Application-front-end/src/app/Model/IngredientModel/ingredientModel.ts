export class IngredientModel {
    constructor(
        public ingredientLibelle: string,
        public ingredientUnite ?: number,
        public ingredientQuantite ?: number,
        public id ?: number
    ){}
}
