import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from "@angular/material/button";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTableModule } from "@angular/material/table";
import { NavbarComponent } from './navbar/navbar.component';
import { RecetteComponent } from './pages/recette/recette.component';
import { IngredientComponent } from './pages/ingredient/list/ingredient.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { HttpClientModule } from '@angular/common/http';
import { AddComponent } from './pages/ingredient/add/add.component';
import { MatInputModule } from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {ReactiveFormsModule} from "@angular/forms";
import { EditComponent } from './pages/ingredient/edit/edit.component';
import { ListComponent } from './pages/repas/list/list.component';
import { SplitPipe } from './pipes/split.pipe';
import { AddMealComponent } from './pages/repas/add-meal/add-meal.component';
import { EditMealComponent } from './pages/repas/edit-meal/edit-meal.component';
import { ShowComponent } from './pages/repas/show/show.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RecetteComponent,
    IngredientComponent,
    AddComponent,
    EditComponent,
    ListComponent,
    SplitPipe,
    AddMealComponent,
    EditMealComponent,
    ShowComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
