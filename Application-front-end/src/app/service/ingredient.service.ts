import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { INGREDIENT_URL } from '../utils/links';
import { HTTP_HEADERS } from '../utils/header';


@Injectable({
  providedIn: 'root'
})
export class IngredientService {

  constructor(private http: HttpClient) { }

 /**
  * Recupère la liste des ingredients
  */
  getAllIngredientsService()
  {
    return this.http.get(INGREDIENT_URL,HTTP_HEADERS).toPromise();
  }

  /**
   * Edite un ingredient
   */
  getIngredientService(id:number)
  {
    return this.http.get(`${INGREDIENT_URL}/${id}`,HTTP_HEADERS).toPromise();
  }

  /**
   * Ajoute un ingredient
   * @param data 
   */
  addIngredientService(data:any)
  {
    return this.http.post(INGREDIENT_URL,data,HTTP_HEADERS).toPromise();
  }

  /**
   * Modifie un ingredient
   * @param data 
   * @param id 
   */
  updateIngredientService(data:any, id:number)
  {
    return this.http.put(`${INGREDIENT_URL}/${id}`,data,HTTP_HEADERS).toPromise();
  }

  /**
   * Supprimer un ingredient
   * @param id 
   */
  deleteIngredientService(id:number)
  {
    return this.http.delete(`${INGREDIENT_URL}/${id}`,HTTP_HEADERS).toPromise();
  }
}
