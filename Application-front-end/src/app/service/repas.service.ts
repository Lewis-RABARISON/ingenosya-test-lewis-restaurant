import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { REPAS_URL } from '../utils/links';
import { HTTP_HEADERS } from '../utils/header';


@Injectable({
  providedIn: 'root'
})
export class RepasService {

  constructor(private http: HttpClient) { }

  /**
  * Recupère la liste des Repas
  */
   getAllRepasService()
   {
     return this.http.get(REPAS_URL,HTTP_HEADERS).toPromise();
   }
 
   /**
    * Edite un repas
    */
   getRepasService(id:number)
   {
     return this.http.get(`${REPAS_URL}/${id}`,HTTP_HEADERS).toPromise();
   }
 
   /**
    * Ajoute un repas
    * @param data 
    */
   addRepasService(data:any)
   {
     return this.http.post(REPAS_URL,data,HTTP_HEADERS).toPromise();
   }
 
   /**
    * Modifie un repas
    * @param data 
    * @param id 
    */
   updateRepasService(data:any, id:number)
   {
     return this.http.put(`${REPAS_URL}/${id}`,data,HTTP_HEADERS).toPromise();
   }
 
   /**
    * Supprimer un repas
    * @param id 
    */
   deleteRepasService(id:number)
   {
     return this.http.delete(`${REPAS_URL}/${id}`,HTTP_HEADERS).toPromise();
   }
}
