import { HttpHeaders } from "@angular/common/http";

export const HTTP_HEADERS  = {
    headers : new HttpHeaders({
        'Content-type' : 'application/json'
    })
};