<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RepasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=RepasRepository::class)
 * @ApiResource(
 *     formats={"json"},
 *     collectionOperations={"GET","POST"},
 *     itemOperations={"GET","PUT","DELETE"},
 *     normalizationContext={"groups"={"meal_read"}}
 * )
 */
class Repas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"meal_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"meal_read","ingredient_read"})
     * @Assert\NotBlank()
     * @Assert\Length(min=0)
     */
    private $repasPrix;

    /**
     * @ORM\ManyToMany(targetEntity=Ingredient::class, inversedBy="repas")
     * @Groups({"meal_read","ingredient_read"})
     */
    private $ingredient;

    /**
     * @ORM\Column(type="string", length=70)
     * @Groups({"meal_read"})
     */
    private $repasDesignation;

    public function __construct()
    {
        $this->ingredient = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRepasPrix(): ?int
    {
        return $this->repasPrix;
    }

    public function setRepasPrix(int $repasPrix): self
    {
        $this->repasPrix = $repasPrix;

        return $this;
    }

    /**
     * @return Collection|Ingredient[]
     */
    public function getIngredient(): Collection
    {
        return $this->ingredient;
    }

    public function addIngredient(Ingredient $ingredient): self
    {
        if (!$this->ingredient->contains($ingredient)) {
            $this->ingredient[] = $ingredient;
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): self
    {
        $this->ingredient->removeElement($ingredient);

        return $this;
    }

    public function getRepasDesignation(): ?string
    {
        return $this->repasDesignation;
    }

    public function setRepasDesignation(string $repasDesignation): self
    {
        $this->repasDesignation = $repasDesignation;

        return $this;
    }
}
