<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\IngredientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=IngredientRepository::class)
 * @ApiResource(
 *     formats={"json"},
 *     collectionOperations={"GET","POST"},
 *     itemOperations={"GET","PUT","DELETE"},
 *     normalizationContext={"groups"={"ingredient_read"}}
 * )
 * @UniqueEntity("ingredientLibelle")
 */
class Ingredient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"ingredient_read","meal_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=70, unique=true)
     * @Assert\NotBlank()
     * @Groups({"ingredient_read","meal_read"})
     */
    private $ingredientLibelle;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Groups({"ingredient_read","meal_read"})
     */
    private $ingredientUnite;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Length(min=0)
     * @Groups({"ingredient_read","meal_read"})
     */
    private $ingredientQuantite = 0;

    /**
     * @ORM\ManyToMany(targetEntity=Repas::class, mappedBy="ingredient")
     */
    private $repas;

    public function __construct()
    {
        $this->repas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIngredientLibelle(): ?string
    {
        return $this->ingredientLibelle;
    }

    public function setIngredientLibelle(string $ingredientLibelle): self
    {
        $this->ingredientLibelle = $ingredientLibelle;

        return $this;
    }

    public function getIngredientUnite(): ?string
    {
        return $this->ingredientUnite;
    }

    public function setIngredientUnite(?string $ingredientUnite): self
    {
        $this->ingredientUnite = $ingredientUnite;

        return $this;
    }

    public function getIngredientQuantite(): ?float
    {
        return $this->ingredientQuantite;
    }

    public function setIngredientQuantite(?float $ingredientQuantite): self
    {
        $this->ingredientQuantite = $ingredientQuantite;

        return $this;
    }

    /**
     * @return Collection|Repas[]
     */
    public function getRepas(): Collection
    {
        return $this->repas;
    }

    public function addRepa(Repas $repa): self
    {
        if (!$this->repas->contains($repa)) {
            $this->repas[] = $repa;
            $repa->addIngredient($this);
        }

        return $this;
    }

    public function removeRepa(Repas $repa): self
    {
        if ($this->repas->removeElement($repa)) {
            $repa->removeIngredient($this);
        }

        return $this;
    }
}
